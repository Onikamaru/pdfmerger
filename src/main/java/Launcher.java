import org.apache.pdfbox.multipdf.PDFMergerUtility;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Launcher {

    public void launch(){
        File[] files = chooseFiles();
        if(files==null){
            return;
        }
        String pdfName = userQuery();
        mergePDF(files, pdfName);

    }

    private String userQuery() {
        System.out.println("Please enter the name of the merged pdf. The .pdf suffix is not needed.");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine() + ".pdf";
    }

    public File[] chooseFiles(){
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Pdfs","pdf");
        fc.setFileFilter(filter);
        fc.setMultiSelectionEnabled(true);

        int response = fc.showOpenDialog(null);

        if(response== JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFiles();
        }else{
            return null;
        }
    }

    public void mergePDF(File[] files, String pdfName){
        PDFMergerUtility pdfUtility = new PDFMergerUtility();
        pdfUtility.setDestinationFileName(files[0].getPath().substring(0,files[0].getPath().lastIndexOf("\\"))+"\\"+pdfName);
        for (File file : files) {
            try {
                pdfUtility.addSource(file);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            //noinspection deprecation
            pdfUtility.mergeDocuments();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
